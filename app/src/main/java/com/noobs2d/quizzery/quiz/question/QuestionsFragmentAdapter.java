package com.noobs2d.quizzery.quiz.question;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.noobs2d.quizzery.util.Keys;
import com.noobs2d.quizzery.model.Question;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class QuestionsFragmentAdapter extends FragmentStatePagerAdapter {

    public static final String TAG = QuestionsFragmentAdapter.class.getSimpleName();

    private List<Question> mListQuestions;

    public QuestionsFragmentAdapter(FragmentManager fragmentManager, List<Question> listQuestions) {
        super(fragmentManager);

        mListQuestions = listQuestions;
    }

    @Override
    public Fragment getItem(int position) {
        try {
            Bundle args = new Bundle();
            args.putSerializable(Keys.QUESTION, mListQuestions.get(position));

            QuestionViewImpl questionFragment = new QuestionViewImpl();
            questionFragment.setArguments(args);

            return questionFragment;
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG, "ArrayIndexOutOfBoundsException: " + e.getMessage());
            return null;
        }
    }

    @Override
    public int getCount() {
        return mListQuestions.size();
    }
}
