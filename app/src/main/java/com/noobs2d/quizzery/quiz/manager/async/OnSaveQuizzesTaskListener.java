package com.noobs2d.quizzery.quiz.manager.async;

/**
 * @author Julious Igmen
 */
public interface OnSaveQuizzesTaskListener {

    public void onSaveQuizzesFail();

    public void onSaveQuizzesSuccess();
}
