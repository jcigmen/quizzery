package com.noobs2d.quizzery.quiz;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.framework.QuizzeryFragment;
import com.noobs2d.quizzery.model.Question;
import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.question.QuestionViewImpl;
import com.noobs2d.quizzery.util.Keys;

/**
 * @author Julious Igmen
 */
public class QuizViewImpl extends QuizzeryFragment implements QuizView {

    private Quiz mQuiz;
    private QuizPresenter mPresenterQuiz;
    private RecyclerView mRecyclerViewQuestions;

    @Override
    public String getScreenTitle() {
        return mQuiz.name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quiz, container, false);

        initializePresenter();
        initializeViews(rootView);

        return rootView;
    }

    private void initializePresenter() {
        mPresenterQuiz = new QuizPresenterImpl(this);
    }

    private void initializeViews(View rootView) {
        QuestionsAdapter adapterQuestions = new QuestionsAdapter(mQuiz.questions, mPresenterQuiz);

        mRecyclerViewQuestions = (RecyclerView) rootView.findViewById(R.id.quiz_recyclerview_questions);
        mRecyclerViewQuestions.setAdapter(adapterQuestions);
        mRecyclerViewQuestions.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewQuestions.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mQuiz = (Quiz) getArguments().getSerializable(Keys.QUIZ);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();

        inflater.inflate(R.menu.menu_quiz, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean addQuizMenuClicked = item.getItemId() == R.id.action_quiz_add;
        if (addQuizMenuClicked) {
            navigateToAddQuestionScreen();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        super.onBackStackChanged();

        mRecyclerViewQuestions.getAdapter().notifyDataSetChanged();
    }

    private void navigateToAddQuestionScreen() {
        Question question = new Question("");

        mQuiz.questions.add(question);

        navigateToQuestionEditScreen(question);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mPresenterQuiz.saveQuiz(mQuiz);
    }

    @Override
    public void navigateToQuestionEditScreen(Question question) {
        Bundle args = new Bundle();
        args.putSerializable(Keys.QUESTION, question);
        args.putInt(QuestionViewImpl.BUNDLE_KEY_MODE, QuestionViewImpl.MODE_ADD_CHOICES);

        QuestionViewImpl questionFragment = new QuestionViewImpl();
        questionFragment.setArguments(args);

        getScreenNavigator().addScreen(questionFragment);
    }

}
