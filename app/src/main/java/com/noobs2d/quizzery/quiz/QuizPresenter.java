package com.noobs2d.quizzery.quiz;

import android.content.DialogInterface;
import android.view.View;

import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public interface QuizPresenter extends QuestionsAdapter.OnQuestionAdapterItemClickListener {

    public void saveQuiz(Quiz quiz);
}
