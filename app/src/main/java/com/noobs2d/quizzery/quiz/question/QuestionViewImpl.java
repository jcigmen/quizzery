package com.noobs2d.quizzery.quiz.question;


import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.noobs2d.quizzery.framework.ScreenNavigator;
import com.noobs2d.quizzery.model.Choice;
import com.noobs2d.quizzery.quiz.question.choice.CreateChoiceDialogPresenter;
import com.noobs2d.quizzery.quiz.question.choice.CreateChoiceDialogViewImpl;
import com.noobs2d.quizzery.util.Keys;
import com.noobs2d.quizzery.framework.QuizzeryFragment;
import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Question;


/**
 * @author Julious Igmen
 */
public class QuestionViewImpl extends QuizzeryFragment implements QuestionView {

    public static final String BUNDLE_KEY_MODE = "BKM";

    public static final int MODE_ADD_CHOICES = 0x51;
    public static final int MODE_ANSWER_QUESTION = 0x52;

    private int mMode;
    private QuestionPresenter mPresenterQuestion;
    private RecyclerView mRecyclerViewChoices;
    private Question mQuestion;

    @Override
    public void uncheckAllChoices() {
        for (Choice choice : mQuestion.choices) {
            choice.isCorrectAnswer = false;
        }
    }

    @Override
    public void uncheckOtherChoices(int exception) {
        for (int i = 0; i < mQuestion.choices.size(); i++) {
            mQuestion.choices.get(i).isCorrectAnswer = i == exception;
        }
    }

    @Override
    public void showCreateChoiceDialog() {
        Bundle args = new Bundle();
        args.putSerializable(Keys.BUNDLE_KEY_LISTENER, mPresenterQuestion);

        CreateChoiceDialogViewImpl createChoiceDialog = new CreateChoiceDialogViewImpl();
        createChoiceDialog.setArguments(args);

        ScreenNavigator screenNavigator = getScreenNavigator();
        screenNavigator.showDialog(createChoiceDialog);
    }

    @Override
    public void addChoice(Choice choice) {
        mQuestion.choices.add(choice);
    }

    @Override
    public void updateChoicesList() {
        mRecyclerViewChoices.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void scrollDownToNewestChoice() {
        int newestChoicePosition = mRecyclerViewChoices.getAdapter().getItemCount() - 1;
        mRecyclerViewChoices.scrollToPosition(newestChoicePosition);
    }

    @Override
    public String getScreenTitle() {
        Question question = (Question) getArguments().getSerializable(Keys.QUESTION);

        if (question.query.isEmpty()) {
            return getString(R.string.add_question);
        } else {
            return getString(R.string.edit_question);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();

        inflater.inflate(R.menu.menu_question, menu);

        // TODO hide the delete menu if on add mode
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean addMenuClicked = item.getItemId() == R.id.action_question_add;
        if (addMenuClicked) {
            showCreateChoiceDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question, container, false);

        initQuestion();
        initPresenter();
        initMode();
        initViews(rootView);

        return rootView;
    }

    private void initPresenter() {
        mPresenterQuestion = new QuestionPresenterImpl(this, mQuestion);
    }

    private void initQuestion() {
        mQuestion = (Question) getArguments().getSerializable(Keys.QUESTION);
    }

    private void initMode() {
        mMode = getArguments().getInt(BUNDLE_KEY_MODE, MODE_ADD_CHOICES);
    }

    private void initViews(View rootView) {
        EditText editTextQuery = (EditText) rootView.findViewById(R.id.question_textview_query);
        editTextQuery.setText(mQuestion.query);
        editTextQuery.addTextChangedListener(mPresenterQuestion);

        ChoicesAdapter adapter;

        switch (mMode) {
            case MODE_ADD_CHOICES:
                adapter = new ChoicesAdapter(mQuestion.choices, mPresenterQuestion, true);
                break;
            default:
                adapter = new ChoicesAdapter(mQuestion.choices, mPresenterQuestion, false);
        }

        mRecyclerViewChoices = (RecyclerView) rootView.findViewById(R.id.question_recyclerview_choices);
        mRecyclerViewChoices.setAdapter(adapter);
        mRecyclerViewChoices.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewChoices.setItemAnimator(new DefaultItemAnimator());
    }
}
