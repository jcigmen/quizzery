package com.noobs2d.quizzery.quiz.manager.async;

import com.noobs2d.quizzery.model.Quiz;

import java.util.List;

/**
 * @author Julious Igmen
 */
public interface OnLoadQuizzesTaskListener {

    public void onLoadQuizzesFail();

    public void onLoadQuizzesSuccess(List<Quiz> quizzes);
}
