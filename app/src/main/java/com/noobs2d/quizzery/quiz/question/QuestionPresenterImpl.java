package com.noobs2d.quizzery.quiz.question;

import android.text.Editable;
import android.view.View;

import com.noobs2d.quizzery.model.Choice;
import com.noobs2d.quizzery.model.Question;

/**
 * @author Julious Igmen
 */
public class QuestionPresenterImpl implements QuestionPresenter {

    private QuestionView mViewQuestion;
    private Question mQuestion;

    public QuestionPresenterImpl(QuestionView viewQuestion, Question question) {
        mViewQuestion = viewQuestion;
        mQuestion = question;
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onChoiceCreate(Choice choice) {
        if (choice.isCorrectAnswer) {
            mViewQuestion.uncheckAllChoices();
        }

        createNewChoice(choice);
    }

    @Override
    public void onChoiceClick(int position) {
        mViewQuestion.uncheckOtherChoices(position);
        mViewQuestion.updateChoicesList();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mQuestion.query = "" + s;
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void createNewChoice(Choice choice) {
        mViewQuestion.addChoice(choice);
        mViewQuestion.updateChoicesList();
        mViewQuestion.scrollDownToNewestChoice();
    }
}
