package com.noobs2d.quizzery.quiz.create;

import android.content.DialogInterface;

/**
 * @author Julious Igmen
 */
public interface CreateQuizDialogView extends DialogInterface.OnShowListener {

    public String getQuizNameFieldValue();

    public String getQuizDescriptionFieldValue();

    public void setCreateButtonEnabled(boolean enabled);
}
