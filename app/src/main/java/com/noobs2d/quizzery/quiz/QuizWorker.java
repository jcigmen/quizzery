package com.noobs2d.quizzery.quiz;

import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public interface QuizWorker {

    public void saveQuiz(Quiz quiz);
}
