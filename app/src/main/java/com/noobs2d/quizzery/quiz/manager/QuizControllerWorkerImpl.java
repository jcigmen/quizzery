package com.noobs2d.quizzery.quiz.manager;

import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.manager.async.LoadQuizzesTask;
import com.noobs2d.quizzery.quiz.manager.async.OnLoadQuizzesTaskListener;
import com.noobs2d.quizzery.quiz.manager.async.OnSaveQuizzesTaskListener;
import com.noobs2d.quizzery.quiz.manager.async.SaveQuizzesTask;

/**
 * @author Julious Igmen
 */
public class QuizControllerWorkerImpl implements QuizControllerWorker {

    private OnLoadQuizzesTaskListener mListenerOnLoadQuizzesTask;
    private OnSaveQuizzesTaskListener mListenerOnSaveQuizzesTask;

    private LoadQuizzesTask mLoadQuizzesTask;

    public QuizControllerWorkerImpl(OnLoadQuizzesTaskListener listenerOnLoadQuizzesTask, OnSaveQuizzesTaskListener listenerOnSaveQuizzesTask) {
        this.mListenerOnLoadQuizzesTask = listenerOnLoadQuizzesTask;
        this.mListenerOnSaveQuizzesTask = listenerOnSaveQuizzesTask;

        mLoadQuizzesTask = new LoadQuizzesTask(listenerOnLoadQuizzesTask);
    }

    @Override
    public void loadQuizzes() {
        mLoadQuizzesTask.execute();
    }

    @Override
    public void saveQuiz(Quiz quiz) {
        SaveQuizzesTask task = new SaveQuizzesTask(mListenerOnSaveQuizzesTask);
        task.execute(quiz);
    }
}
