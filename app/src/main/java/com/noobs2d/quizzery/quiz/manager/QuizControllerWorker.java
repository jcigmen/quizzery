package com.noobs2d.quizzery.quiz.manager;

import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public interface QuizControllerWorker {

    public void loadQuizzes();

    public void saveQuiz(Quiz quiz);
}
