package com.noobs2d.quizzery.quiz.question.choice;

import android.content.DialogInterface;

/**
 * @author Julious Igmen
 */
public interface CreateChoiceDialogView extends DialogInterface.OnShowListener {

    public String getChoiceName();

    public boolean isCorrectAnswerCheckboxChecked();

    public void setCreateButtonEnabled(boolean enabled);
}
