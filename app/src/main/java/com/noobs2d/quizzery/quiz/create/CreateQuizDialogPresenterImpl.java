package com.noobs2d.quizzery.quiz.create;

import android.content.DialogInterface;
import android.text.Editable;

import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public class CreateQuizDialogPresenterImpl implements CreateQuizDialogPresenter {

    private CreateQuizDialogView mViewCreateQuizDialog;
    private OnCreateQuizDialogListener mListenerOnCreateQuizDialog;

    public CreateQuizDialogPresenterImpl(CreateQuizDialogView viewCreateQuizDialog, OnCreateQuizDialogListener listenerCreateQuizDialog) {
        this.mViewCreateQuizDialog = viewCreateQuizDialog;
        this.mListenerOnCreateQuizDialog = listenerCreateQuizDialog;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        boolean quizNameFieldIsEmpty = count == 0;

        if (quizNameFieldIsEmpty) {
            mViewCreateQuizDialog.setCreateButtonEnabled(false);
        } else {
            mViewCreateQuizDialog.setCreateButtonEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        boolean createButtonClicked = which == DialogInterface.BUTTON_POSITIVE;
        if (createButtonClicked) {
            invokeListenerEvent();
        }
    }

    private void invokeListenerEvent() {
        String quizName = mViewCreateQuizDialog.getQuizNameFieldValue();
        String quizDescription = mViewCreateQuizDialog.getQuizDescriptionFieldValue();
        Quiz quiz = new Quiz(quizName, quizDescription);

        mListenerOnCreateQuizDialog.onQuizCreate(quiz);
    }
}
