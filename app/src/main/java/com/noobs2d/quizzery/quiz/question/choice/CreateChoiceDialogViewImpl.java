package com.noobs2d.quizzery.quiz.question.choice;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.util.Keys;

/**
 * @author Julious Igmen
 */
public class CreateChoiceDialogViewImpl extends DialogFragment implements CreateChoiceDialogView {

    private Button mButtonCreate;
    private EditText mEditTextChoice;
    private CheckBox mCheckBoxCorrectAnswer;
    private CreateChoiceDialogPresenter mPresenterCreateChoiceDialog;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

        View rootView = layoutInflater.inflate(R.layout.fragment_dialog_choice_add, null, false);

        initPresenter();
        initViews(rootView);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.add_choice));
        builder.setView(rootView);
        builder.setPositiveButton(R.string.create, mPresenterCreateChoiceDialog);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(this);

        return dialog;
    }

    private void initPresenter() {
        OnCreateChoiceListener listenerCreateChoiceDialog = (OnCreateChoiceListener) getArguments().getSerializable(Keys.BUNDLE_KEY_LISTENER);
        if (listenerCreateChoiceDialog == null) {
            throw new RuntimeException("Each CreateChoiceDialog must have a listener!");
        }

        mPresenterCreateChoiceDialog = new CreateChoiceDialogPresenterImpl(this, listenerCreateChoiceDialog);
    }

    private void initViews(View rootView) {
        mEditTextChoice = (EditText) rootView.findViewById(R.id.choice_add_edittext_choice);
        mEditTextChoice.addTextChangedListener(mPresenterCreateChoiceDialog);

        mCheckBoxCorrectAnswer = (CheckBox) rootView.findViewById(R.id.choice_add_checkbox_correct_answer);
    }

    @Override
    public String getChoiceName() {
        return mEditTextChoice.getText().toString();
    }

    @Override
    public boolean isCorrectAnswerCheckboxChecked() {
        return mCheckBoxCorrectAnswer.isChecked();
    }

    @Override
    public void setCreateButtonEnabled(boolean enabled) {
        mButtonCreate.setEnabled(enabled);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        AlertDialog alertDialog = ((AlertDialog) getDialog());

        // can be done in the presenter, but since it's only a view-related init-event, it's tolerable to do it here
        mButtonCreate = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mButtonCreate.setEnabled(false);
    }
}
