package com.noobs2d.quizzery.quiz.answer;


import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noobs2d.quizzery.quiz.question.QuestionsFragmentAdapter;
import com.noobs2d.quizzery.util.Keys;
import com.noobs2d.quizzery.framework.QuizzeryFragment;
import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Quiz;


/**
 * @author Julious Igmen
 */
public class AnswerQuizViewImpl extends QuizzeryFragment {

    public static final String TAG = AnswerQuizViewImpl.class.getSimpleName();

    private ViewPager mViewPagerQuestions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Quiz quiz = (Quiz) getArguments().getSerializable(Keys.QUIZ);

        Log.d(TAG, "Quiz \'" + quiz.name + "\' has " + quiz.questions.size() + " questions.");

        View rootView = inflater.inflate(R.layout.fragment_answer_quiz, container, false);

        mViewPagerQuestions = (ViewPager) rootView.findViewById(R.id.answer_quiz_viewpager);
        mViewPagerQuestions.setAdapter(new QuestionsFragmentAdapter(getActivity().getSupportFragmentManager(), quiz.questions));
        mViewPagerQuestions.setOffscreenPageLimit(3);

        return rootView;
    }

    @Override
    public String getScreenTitle() {
        return "Answer Quiz"; // FIXME
    }
}
