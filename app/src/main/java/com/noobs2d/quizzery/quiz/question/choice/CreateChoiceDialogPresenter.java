package com.noobs2d.quizzery.quiz.question.choice;

import android.content.DialogInterface;
import android.text.TextWatcher;

/**
 * @author Julious Igmen
 */
public interface CreateChoiceDialogPresenter extends DialogInterface.OnClickListener, TextWatcher {
}
