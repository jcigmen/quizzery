package com.noobs2d.quizzery.quiz;

import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.manager.async.SaveQuizzesTask;

/**
 * @author Julious Igmen
 */
public class QuizWorkerImpl implements QuizWorker {

    @Override
    public void saveQuiz(Quiz quiz) {
        SaveQuizzesTask task = new SaveQuizzesTask(null);
        task.execute(quiz);
    }
}
