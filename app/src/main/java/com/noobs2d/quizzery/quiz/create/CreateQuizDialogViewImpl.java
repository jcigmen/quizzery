package com.noobs2d.quizzery.quiz.create;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.util.Keys;

/**
 * @author Julious Igmen
 */
public class CreateQuizDialogViewImpl extends DialogFragment implements CreateQuizDialogView {

    public static final String TAG = CreateQuizDialogViewImpl.class.getSimpleName();

    private Button mButtonCreate;
    private EditText mEditTextQuizName;
    private EditText mEditTextQuizDescription;
    private CreateQuizDialogPresenter mPresenterCreateQuizDialog;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

        View rootView = layoutInflater.inflate(R.layout.fragment_dialog_quiz_add, null, false);

        initPresenter();
        initViews(rootView);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.create_quiz);
        builder.setView(rootView);
        builder.setPositiveButton(R.string.create, mPresenterCreateQuizDialog);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(this);

        return dialog;
    }

    private void initPresenter() {
        OnCreateQuizDialogListener listenerCreateQuizDialog = (OnCreateQuizDialogListener) getArguments().getSerializable(Keys.BUNDLE_KEY_LISTENER);
        if (listenerCreateQuizDialog == null) {
            throw new RuntimeException("Each CreateQuizDialog must have a listener!");
        }

        mPresenterCreateQuizDialog = new CreateQuizDialogPresenterImpl(this, listenerCreateQuizDialog);

    }

    private void initViews(View rootView) {
        mEditTextQuizName = (EditText) rootView.findViewById(R.id.quiz_add_edittext_quiz_name);
        mEditTextQuizName.addTextChangedListener(mPresenterCreateQuizDialog);

        mEditTextQuizDescription = (EditText) rootView.findViewById(R.id.quiz_add_edittext_quiz_description);
        mEditTextQuizDescription.addTextChangedListener(mPresenterCreateQuizDialog);

    }

    @Override
    public String getQuizNameFieldValue() {
        return mEditTextQuizName.getText().toString();
    }

    @Override
    public String getQuizDescriptionFieldValue() {
        return mEditTextQuizDescription.getText().toString();
    }

    @Override
    public void setCreateButtonEnabled(boolean enabled) {
        mButtonCreate.setEnabled(enabled);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        AlertDialog alertDialog = ((AlertDialog) getDialog());

        // can be done in the presenter, but since it's only a view-related init-event, it's tolerable to do it here
        mButtonCreate = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mButtonCreate.setEnabled(false);
    }
}
