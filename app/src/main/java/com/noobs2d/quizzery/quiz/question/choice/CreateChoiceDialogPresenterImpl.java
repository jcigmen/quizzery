package com.noobs2d.quizzery.quiz.question.choice;

import android.content.DialogInterface;
import android.text.Editable;

import com.noobs2d.quizzery.model.Choice;

/**
 * @author Julious Igmen
 */
public class CreateChoiceDialogPresenterImpl implements CreateChoiceDialogPresenter {

    private CreateChoiceDialogView mViewCreateChoiceDialog;
    private OnCreateChoiceListener mListenerCreateChoiceDialog;

    public CreateChoiceDialogPresenterImpl(CreateChoiceDialogView viewCreateChoiceDialog, OnCreateChoiceListener listenerCreateChoiceDialog) {
        mViewCreateChoiceDialog = viewCreateChoiceDialog;
        mListenerCreateChoiceDialog = listenerCreateChoiceDialog;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        boolean createButtonClicked = which == DialogInterface.BUTTON_POSITIVE;
        if (createButtonClicked) {
            invokeListenerEvent();
        }
    }

    private void invokeListenerEvent() {
        String choiceText = mViewCreateChoiceDialog.getChoiceName();
        boolean isCorrectAnswer = mViewCreateChoiceDialog.isCorrectAnswerCheckboxChecked();
        Choice choice = new Choice(choiceText, isCorrectAnswer);

        mListenerCreateChoiceDialog.onChoiceCreate(choice);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        boolean hasNoChoiceInputYet = count == 0;

        if (hasNoChoiceInputYet) {
            mViewCreateChoiceDialog.setCreateButtonEnabled(false);
        } else {
            mViewCreateChoiceDialog.setCreateButtonEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
