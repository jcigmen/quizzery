package com.noobs2d.quizzery.util;

/**
 * @author Julious Igmen
 */
public class Keys {

    public static final String BUNDLE_KEY_LISTENER = "akl";
    public static final String QUIZ = "KEY_QUIZ";
    public static final String QUESTION = "KEY_QUESTION";
}
