package com.noobs2d.quizzery.framework;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.noobs2d.quizzery.MainActivity;

/**
 * @author Julious Igmen
 */
public abstract class QuizzeryFragment extends Fragment implements FragmentManager.OnBackStackChangedListener {

    public abstract String getScreenTitle();

    @Override
    public void onResume() {
        super.onResume();

        changeActionBarTitle(getScreenTitle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        // when the user returns to this screen/fragment after pressing back, we retun the title of the toolbar
        if (isVisible()) {
            changeActionBarTitle(getScreenTitle());
        }
    }

    private void changeActionBarTitle(String title) {
        getScreenNavigator().setToolbarTitle(title);
    }

    public ScreenNavigator getScreenNavigator() {
        return (MainActivity) getActivity();
    }
}
