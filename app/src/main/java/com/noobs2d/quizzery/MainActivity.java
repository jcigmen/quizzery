package com.noobs2d.quizzery;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.noobs2d.quizzery.framework.ScreenNavigator;
import com.noobs2d.quizzery.home.QuizzesViewImpl;
import com.noobs2d.quizzery.quiz.manager.QuizController;
import com.noobs2d.quizzery.util.DatabaseController;


public class MainActivity extends ActionBarActivity implements FragmentManager.OnBackStackChangedListener, ScreenNavigator {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    public void navigateToScreen(Fragment screen) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, screen);
        fragmentTransaction.commit();
    }

    @Override
    public void removeAllOtherScreens() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showDialog(DialogFragment dialog) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        dialog.show(fragmentManager, dialog.getTag());
    }

    @Override
    public void addScreen(Fragment screen) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, screen, screen.getTag());
        fragmentTransaction.addToBackStack(screen.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        } else {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        DatabaseController.destroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String databaseName = "quizzery.db";
        int databaseVersion = 1;
        DatabaseController.init(this, databaseName, databaseVersion);
        QuizController.init();

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState == null) {
            navigateToScreen(new QuizzesViewImpl());
        }
    }
}
