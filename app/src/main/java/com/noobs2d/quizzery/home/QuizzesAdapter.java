package com.noobs2d.quizzery.home;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Quiz;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class QuizzesAdapter extends RecyclerView.Adapter<QuizzesAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textViewQuizName;
        public TextView textViewQuizDescription;
        public TextView textViewQuestionCount;

        private List<Quiz> mListQuizzes;
        private QuizzesView mViewQuizzes;

        public ViewHolder(List<Quiz> quizzes, View rootView, QuizzesView viewQuizzes) {
            super(rootView);
            textViewQuizName = (TextView) rootView.findViewById(R.id.quiz_adapter_textview_name);
            textViewQuizDescription = (TextView) rootView.findViewById(R.id.quiz_adapter_textview_description);
            textViewQuestionCount = (TextView) rootView.findViewById(R.id.quiz_adapter_textview_question_count);

            CardView cardView = (CardView) rootView.findViewById(R.id.quiz_adapter_cardview);
            cardView.setOnClickListener(this);

            mListQuizzes = quizzes;
            mViewQuizzes = viewQuizzes;
        }

        @Override
        public void onClick(View view) {
            // FIXME refactor this. only Presenters should call and reference a View
            mViewQuizzes.navigateToAddQuestionsScreen(mListQuizzes.get(getPosition()));
        }
    }

    private List<Quiz> mListQuizzes;
    private QuizzesView mViewQuizzes;

    public QuizzesAdapter(List<Quiz> quizzes, QuizzesView viewQuizzes) {
        mListQuizzes = quizzes;
        mViewQuizzes = viewQuizzes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View rootView = layoutInflater.inflate(R.layout.adapter_quizzes, parent, false);

        return new ViewHolder(mListQuizzes, rootView, mViewQuizzes);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.textViewQuizName.setText(mListQuizzes.get(i).name);

        boolean hasDescription = !mListQuizzes.get(i).description.isEmpty();

        if (hasDescription) {
            viewHolder.textViewQuizDescription.setText(mListQuizzes.get(i).description);
            viewHolder.textViewQuizDescription.setVisibility(View.VISIBLE);
        } else {
            viewHolder.textViewQuizDescription.setVisibility(View.GONE);
        }

        viewHolder.textViewQuestionCount.setText("" + mListQuizzes.get(i).questions.size());
    }

    @Override
    public int getItemCount() {
        return mListQuizzes.size();
    }
}
