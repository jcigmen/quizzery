package com.noobs2d.quizzery.home;

import android.util.Log;
import android.view.View;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.manager.QuizController;
import com.noobs2d.quizzery.quiz.manager.QuizControllerWorker;
import com.noobs2d.quizzery.quiz.manager.QuizControllerWorkerImpl;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class QuizzesPresenterImpl implements QuizzesPresenter {

    public static final String TAG = QuizzesPresenterImpl.class.getSimpleName();

    private QuizzesView viewQuizzes;
    private QuizControllerWorker workerQuizController;

    public QuizzesPresenterImpl(QuizzesView viewQuizzes) {
        this.viewQuizzes = viewQuizzes;
        this.workerQuizController = new QuizControllerWorkerImpl(this, this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quizzes_fab_add:
                viewQuizzes.showCreateQuizDialog();
                break;
        }
    }

    @Override
    public void onQuizCreate(Quiz quiz) {
        Log.d(TAG, "Created quiz named \"" + quiz.name + "\".");

        // show the newly created quiz
        QuizController.quizzes.add(quiz);

        // save the newly created quiz. only after this succeeds should we check to show the list
        workerQuizController.saveQuiz(quiz);
    }

    @Override
    public void onLoadQuizzesFail() {
        Log.d(TAG, "Failed to loaded quizzes!");
        showQuizzesIfHasOne();
    }

    @Override
    public void loadQuizzes() {
        Log.d(TAG, "Loading quizzes...");
        viewQuizzes.setProgressBarVisible(true);
        viewQuizzes.setCreateQuizFabVisible(false);
        workerQuizController.loadQuizzes();
    }

    @Override
    public void onLoadQuizzesSuccess(List<Quiz> quizzes) {
        Log.d(TAG, "Successfully loaded " + quizzes.size() + " quizzes.");

        // show the damn quizzes man!
        QuizController.quizzes.addAll(quizzes);

        showQuizzesIfHasOne();
    }

    @Override
    public void onSaveQuizzesFail() {
        // revert the addition of the last quiz in the queue?
        QuizController.quizzes.remove(QuizController.quizzes.size() - 1);

        showQuizzesIfHasOne();
    }

    private void showNewlySavedQuiz() {
        Quiz quizLastAdded = QuizController.quizzes.get(QuizController.quizzes.size() - 1);

        viewQuizzes.navigateToAddQuestionsScreen(quizLastAdded);
    }

    @Override
    public void onSaveQuizzesSuccess() {
        Log.d(TAG, "Successfully saved a quiz!");
        showNewlySavedQuiz();
    }

    private void showQuizzesIfHasOne() {
        viewQuizzes.setProgressBarVisible(false);
        viewQuizzes.setCreateQuizFabVisible(true);

        boolean hasQuizzes = QuizController.quizzes.size() > 0;
        viewQuizzes.setNoQuizzesPromptVisible(!hasQuizzes);
        viewQuizzes.setQuizzesListVisible(hasQuizzes);
    }
}
