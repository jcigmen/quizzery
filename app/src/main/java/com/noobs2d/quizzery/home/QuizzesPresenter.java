package com.noobs2d.quizzery.home;

import android.view.View;

import com.noobs2d.quizzery.quiz.create.OnCreateQuizDialogListener;
import com.noobs2d.quizzery.quiz.manager.async.OnLoadQuizzesTaskListener;
import com.noobs2d.quizzery.quiz.manager.async.OnSaveQuizzesTaskListener;

/**
 * @author Julious Igmen
 */
public interface QuizzesPresenter extends OnCreateQuizDialogListener, View.OnClickListener,
        OnSaveQuizzesTaskListener, OnLoadQuizzesTaskListener {

    public void loadQuizzes();
}
