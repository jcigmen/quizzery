package com.noobs2d.quizzery.home;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.noobs2d.quizzery.quiz.QuizViewImpl;
import com.noobs2d.quizzery.util.Keys;
import com.noobs2d.quizzery.framework.QuizzeryFragment;
import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.framework.ScreenNavigator;
import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.create.CreateQuizDialogViewImpl;
import com.noobs2d.quizzery.quiz.manager.QuizController;
import com.shamanland.fab.FloatingActionButton;

/**
 * @author Julious Igmen
 */
public class QuizzesViewImpl extends QuizzeryFragment implements QuizzesView {

    private FloatingActionButton mFabCreateQuiz;
    private ProgressBar mProgressBar;
    private QuizzesPresenter mPresenterQuizzes;
    private RecyclerView mRecyclerViewQuizzes;
    private TextView mTextViewNoQuizzes;

    @Override
    public void setQuizzesListVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;

        mRecyclerViewQuizzes.setVisibility(flag);
        mRecyclerViewQuizzes.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void setCreateQuizFabVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;

        mFabCreateQuiz.setVisibility(flag);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quizzes, container, false);

        mPresenterQuizzes = new QuizzesPresenterImpl(this);

        mFabCreateQuiz = (FloatingActionButton) rootView.findViewById(R.id.quizzes_fab_add);
        mFabCreateQuiz.setOnClickListener(mPresenterQuizzes);

        mRecyclerViewQuizzes = (RecyclerView) rootView.findViewById(R.id.quizzes_recyclerview);
        mRecyclerViewQuizzes.setAdapter(new QuizzesAdapter(QuizController.quizzes, this));
        mRecyclerViewQuizzes.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewQuizzes.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewQuizzes.setVisibility(View.GONE);

        mTextViewNoQuizzes = (TextView) rootView.findViewById(R.id.quizzes_textview_no_quizzes);
        mTextViewNoQuizzes.setVisibility(View.GONE);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.quizzes_progressbar);

        return rootView;
    }

    @Override
    public String getScreenTitle() {
        return getString(R.string.app_name);
    }

    @Override
    public void onResume() {
        super.onResume();

        boolean hasNoQuizzes = QuizController.quizzes.size() == 0;

        if (hasNoQuizzes) {
            mPresenterQuizzes.loadQuizzes();
        } else {
            QuizController.quizzes.clear();
            mPresenterQuizzes.loadQuizzes();
        }
    }

    @Override
    public void navigateToAddQuestionsScreen(Quiz quiz) {
        Bundle args = new Bundle();
        args.putSerializable(Keys.QUIZ, quiz);

        QuizViewImpl fragmentQuiz = new QuizViewImpl();
        fragmentQuiz.setArguments(args);

        ScreenNavigator screenNavigator = getScreenNavigator();
        screenNavigator.addScreen(fragmentQuiz);
    }

    @Override
    public void onBackStackChanged() {
        super.onBackStackChanged();

        mRecyclerViewQuizzes.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showCreateQuizDialog() {
        Bundle args = new Bundle();
        args.putSerializable(Keys.BUNDLE_KEY_LISTENER, mPresenterQuizzes);

        CreateQuizDialogViewImpl createQuizDialog = new CreateQuizDialogViewImpl();
        createQuizDialog.setArguments(args);

        ScreenNavigator screenNavigator = getScreenNavigator();
        screenNavigator.showDialog(createQuizDialog);
    }

    @Override
    public void setProgressBarVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;

        mProgressBar.setVisibility(flag);
    }

    @Override
    public void setNoQuizzesPromptVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;

        mTextViewNoQuizzes.setVisibility(flag);
    }
}
