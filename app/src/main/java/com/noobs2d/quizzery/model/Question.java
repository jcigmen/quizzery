package com.noobs2d.quizzery.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Julious Igmen
 */
public class Question implements Serializable {

    public long id;
    public String query;
    public List<Choice> choices;

    public Question(String query) {
        id = -1;
        this.query = query;
        choices = new ArrayList<>();
    }
}
