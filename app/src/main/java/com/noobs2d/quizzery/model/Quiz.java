package com.noobs2d.quizzery.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Julious Igmen
 */
public class Quiz implements Serializable {

    public long id;
    public String name;
    public String description;
    public int type;

    public List<Question> questions;

    public Quiz(String name, String description) {
        id = -1;
        this.name = name;
        this.description = description;
        type = 0;
        questions = new ArrayList<>();

        // FIXME remove later
//        this.description = "Lorem ipsum dolor amet blar bla";
//
//        for (int i = 1; i <= 5; i++) {
//            Question question = new Question("Test Question " + i);
//            question.choices.add(new Choice("Choice A", true));
//            question.choices.add(new Choice("Choice B", false));
//            question.choices.add(new Choice("Choice C", false));
//            question.choices.add(new Choice("Choice D", false));
//
//            questions.add(question);
//        }
    }

}
