package com.noobs2d.quizzery.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Julious Igmen
 */
public class MultipleChoiceQuestion extends Question {

    private List<Choice> mChoices;
    private int mCorrectAnswerIndex;

    public MultipleChoiceQuestion(String query) {
        super(query);

        mChoices = new ArrayList<Choice>();
        mCorrectAnswerIndex = -1;
    }

    public MultipleChoiceQuestion(String query, List<Choice> choices, int correctAnswerIndex) {
        super(query);

        mChoices = choices;
        mCorrectAnswerIndex = correctAnswerIndex;
    }

    public void addChoice(Choice choice) {
        mChoices.add(choice);
    }

    public void setCorrectAnswerIndex(int index) {
        mCorrectAnswerIndex = index;
    }
}
